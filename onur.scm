#!/usr/bin/guile \
--no-auto-compile --debug -C "$PWD" -e (onur) -s
!#

;; Onur is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; make files in $PWD/qas discoverable to guile
(add-to-load-path (dirname (current-filename)))

(define-module (onur)
  #:use-module (ice-9 getopt-long)
  #:use-module (ice-9 textual-ports)
  #:use-module (ice-9 command-line)
  #:use-module (srfi srfi-13)

  #:use-module (onur cli)
  #:use-module (onur commands)
  #:export (main))

;; ======================== MAIN

(define version
  (let* ((f (call-with-input-file "VERSION" get-string-all))
         (len (string-length f)))
    (substring f 0 (- len 1))))

(define global-args (cddr (command-line)))

(define cmds `'(("grab" . ,(make-onur-command "grab" "grab projects"
                                              (lambda () (grab))))
                ("archive" . ,(make-onur-command "archive" "archive selected projects"
                                                 (lambda () (archive global-args))))
                ("config" . ,(make-onur-command "config" "display/manage configurations"
                                                (lambda () (config global-args))))))

(define (main args)
  (let ((arguments (cdr args)) ;; ignore script name
        (commands (cadr cmds))) ;;
    (cli-parser arguments commands)))
