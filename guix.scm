;; Onur is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (onur)
  #:use-module (guix packages)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix gexp)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages texinfo)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 popen)
  #:use-module (gnu packages guile-xyz))

(define-public onur
  (package
    (version "0.3.0")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://codeberg.org/easbarbosa/onur-scheme/archive/"
                                  version ".tar.gz"))
              (sha256
               (base32
                "1ncj4d0r7gvd2rdg93rc51jvm8pnai7h57mrdsr28qvj46w3ihn2"))))
    (build-system gnu-build-system)
    (native-inputs
     (list autoconf
           automake
           pkg-config
           texinfo
           guile-3.0))
    (propagated-inputs
     (list guile-3.0
           guile-json-rpc
           guile-srfi-145
           guile-srfi-180
           guile-irregex))
    (synopsis "Easily manage multiple FLOSS repositories")
    (description "Easily manage multiple FLOSS repositories")
    (home-page "https://codeberg.org/easbarbosa/onur-scheme")
    (license license:gpl3+)))

onur
