;; Onur is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (onur actions)
  #:use-module (ice-9 ftw)
  #:use-module (ice-9 format)
  #:use-module (ice-9 textual-ports)

  #:use-module (json)

  #:export (actions-pull actions-clone))

(define (actions-pull folder branch)
  (system* "git" "-C" folder "fetch" "origin" branch "--quiet" "--depth=1")
  (system* "git" "-C" folder "reset" "--hard" "--quiet"))

(define (actions-clone url branch folder)
  (system* "git" "clone" "--single-branch" "--depth=1" "--quiet" "--no-tags"
           (string-append "--branch=" branch)
           url
           folder))
