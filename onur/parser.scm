;; Onur is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (onur parser)
  #:use-module (ice-9 ftw)
  #:use-module (ice-9 format)
  #:use-module (ice-9 textual-ports)

  #:use-module (json)

  #:use-module (onur repository)
  #:export (parser-single parser-multi))

;; list of all parsed files bundled as alist: name -> content
(define (parser-multi)
  (filenames-print)
  (map parser-single (filenames-list)))

;; return single parsed json file as scheme
(define (parser-single filepath)
  `(list ,(filenames-stem (basename filepath)) . ,(parser filepath)))

(define (parser filepath)
  (let* ((schemed-file (json-string->scm (file-contents filepath))))
    (map (lambda (pair)
           (let* ((topic (car pair))
                  (projects-raw (vector->list(cdr pair)))
                  (projects (map (lambda (f)
                                   `(list
                                     ("name" . ,(cdr (assoc "name" f)))
                                     ("url" . ,(cdr (assoc "url" f)))
                                     ("branch" . ,(if (assoc "branch" f) (cdr (assoc "branch" f)) "master"))))
                                 projects-raw)))
             `(,topic . ,projects)))
         schemed-file)))

;; returns file contents as string
(define (file-contents filepath)
  (call-with-input-file filepath get-string-all))


;; (display (format #f "     ~s ~s ~s\n"
;;                  (cdr (assoc "name" f))
;;                  (cdr (assoc "url" f))
;;                  (if (assoc "branch" f) (cdr (assoc "branch" f)) "master")))
