;; Onur is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (onur commands)
  #:use-module (ice-9 format)

  #:use-module (onur parser)
  #:use-module (onur globals)
  #:use-module (onur actions)
  #:use-module (onur repository)

  #:export (grab archive config))

(define (grab) (topic-each))

(define (topic-each)
  (for-each (lambda (config)
              (let ((cfg-name (cadr config)))
                (display (format #f "\n> ~a" cfg-name))
                (for-each (lambda (value)
                            (let ((topic (car value))
                                  (projects (cdr value)))
                              (display (format #f "\n~2a+ ~a \n" " " topic))
                              (project-each cfg-name topic projects)))
                          (cddr config))))
            (parser-multi)))

(define (project-each config topic projects)
  (for-each (lambda (project)
              (let* ((projekt (cdr project))
                     (name (cdr (assoc "name" projekt)))
                     (url (cdr (assoc "url" projekt)))
                     (branch (cdr (assoc "branch" projekt)))
                     (folder (string-append projects-home "/"
                                            config "/" topic "/" name)))

                (display (format #f
                                 "~4a-~35a ~70a ~a\n" " "
                                 name url branch))

                (if (file-exists? (string-append folder "/" ".git" "/" "config"))
                    (actions-pull folder branch)
                    (actions-clone url branch folder))))
            projects))

(define (archive projects)
  (display (format #f "~s" projects)))

(define (config terms)
  (display (format #f "~s" terms)))
