;; Onur is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (onur repository)
  #:use-module (ice-9 ftw)
  #:use-module (ice-9 format)

  #:use-module (onur globals)

  #:export (filenames-list filenames-print filenames-stem filenames-length))

;; returns absolute path of files, filtered by criteria files.
(define (filenames-list) (filenames))

;; walk recursively through the onur config home and return files found.
(define (filenames) (filter ok? filenames-fullpath))

(define filenames-fullpath
  (map (lambda (fname) (string-append onur-home "/" fname))
       (scandir onur-home)))

(define (ok? fname)
  (and (string-contains fname ".json")
       (file-exists? fname)
       (not (file-empty? fname))))

(define (file-empty? filename)
  (call-with-input-file filename
    (lambda (input-port)
      (let ((contents (read-char input-port)))
        (eof-object? contents)))))

;; print all files
(define (filenames-print)
  (display "configurations: [")
  (for-each (lambda (filename)
              (display (format #f " ~a " (filenames-stem (basename filename)))))
            (filenames-list))
  (display "]\n"))

;; name without extension
(define (filenames-stem filename-basename)
  (substring filename-basename
             0
             (filenames-length filename-basename ".json")))

(define (filenames-length filename-basename extension)
  (- (string-length filename-basename)
     (string-length extension)))

(define filenames-count (length (filenames-list)))
