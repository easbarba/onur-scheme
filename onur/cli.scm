;; Onur is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (onur cli)
  #:use-module (ice-9 ftw)
  #:use-module (ice-9 format)
  #:use-module (srfi srfi-9)

  #:export (cli-parser make-onur-command))

;; ======================== CLI API

(define (cli-usage-banner)
  (display "Usage: onur <command> [flags]

Flags:
  -h, --help     Show context-sensitive help.
      --debug    Enable debug mode.

Commands:
  archive <package> [flags]
    archive selected projects

  grab [<topic>] [flags]
    grab all projects

  config <topic> [<name> [<url> [<branch>]]] [flags]
    Manage configurations

Run \"onur <command> --help\" for more information on a command."))

(define-record-type <onur-command>
  (make-onur-command name description run)
  onur-command?
  (name            onur-command-name)
  (description     onur-command-description    set-onur-command-description!)
  (run             onur-command-run))

(define (cli-parser args commands)
  ;; (display (format #f "~s \n" (car (cdr args))))
  (when (or (null? args)
            (string= "--help" (car args))
            (string= "-h" (car args)))
    (cli-usage-banner)
    (exit 1))

  (let* ((cli-command (car args))
         (cli-arguments (cdr args))
         (cli-flags (cdr args))
         (cli-true (assoc-ref commands cli-command)))

    (when (not cli-true)
      (cli-usage-banner)
      (exit 1))

    ((onur-command-run (assoc-ref commands cli-command)))(assoc-ref commands cli-command)))

;; ======================== CLI PARSING



;; (define cli-option-list '((version   (single-char #\v) (value #f))
;;                           (grab      (single-char #\g) (value #f))
;;                           (archive   (single-char #\a) (value #t))
;;                           (help      (single-char #\h) (value #f))))

;; (define (cli-parser args)
;;   (let* ((option-spec cli-option-list)
;;          (options (getopt-long args option-spec)))
;;     (cli-option-run options)))

;; (define (cli-option-run options)
;;   (let ((option-wanted (lambda (option) (option-ref options option #f))))
;;     (cond [(option-wanted 'version)   (display (version))]
;;           [(option-wanted 'grab)      (grab)]
;;           [(option-wanted 'archive)   (archive options)]
;;           [(option-wanted 'help)      (cli-usage-banner)]
;;           [else                       (cli-usage-banner)])))
